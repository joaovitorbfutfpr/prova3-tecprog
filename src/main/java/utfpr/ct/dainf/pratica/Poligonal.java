package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();
    
    public int getN(){
        return vertices.size();
    }
    
    public T get(int i){
        if (i<vertices.size() && i>=0){
            return vertices.get(i);
        } else {
            throw new RuntimeException("get("+String.valueOf(i)+"): índice inválido");
        }
    }
    
    public void set(int i, T p){
        if (i>=0 && i<vertices.size()){
            vertices.set(i, p);
        } else if (i==vertices.size()){
            vertices.add(i, p);
        } else {
            throw new RuntimeException("set("+String.valueOf(i)+"): índice inválido");
        }
    }
    
    public double getComprimento(){
        double comp = 0.0;
        int i;
        for (i=0; i<vertices.size()-1; i++){
            comp+=vertices.get(i).dist(vertices.get(i+1));
        }
        return comp;
    }
}
