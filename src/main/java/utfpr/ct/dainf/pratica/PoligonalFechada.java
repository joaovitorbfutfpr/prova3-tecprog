/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1949772
 */
public class PoligonalFechada extends Poligonal {
    @Override
    public double getComprimento(){
        double comp = super.getComprimento();
        comp += super.get(super.getN()-1).dist(super.get(0));
        return comp;
    }
}
