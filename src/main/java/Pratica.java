
import utfpr.ct.dainf.pratica.PoligonalFechada;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica {

    public static void main(String[] args) {
        PoligonalFechada poli = new PoligonalFechada();
        poli.set(0, new PontoXZ(-3, 2));
        poli.set(1, new PontoXZ(-3, 6));
        poli.set(2, new PontoXZ(0, 2));
        System.out.print("Comprimento da poligonal = "+String.valueOf(poli.getComprimento()));
    }
    
}
